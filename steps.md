v1 - Create a simple wireguard network between 2 hosts
v2 - Idempotent playbook
v3 - Improve readability
v4 - Add handler: reload configuration on change
v5 - Inventory agnostic role - support multiples wireguard networks in inventory
v6 - Prevent to leak private keys
v7 - Add a verify entrypoint
v8 - Add a meta folder with argument_specs, main and requirements
v9 - Add molecule with 2 senarii
v10 - Add a custom filter plugin and modify argument_specs accordingly
v11 - Add ansible-lint and correct errors
v12 - Generate defaults and README from metadata