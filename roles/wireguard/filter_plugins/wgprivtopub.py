# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
  name: wgpriv2pub
  short_description: Get wireguard public key from a private one
  version_added: 1.0.0
  author: Manuel Luzarreta <manuel.luzarreta@clever-cloud.com>
  description:
    - Get wireguard public key from a private one
  options:
    _input:
      description: A wireguard private key
      type: str
      required: true
'''

EXAMPLES = '''
- name: Get a wg public key
  ansible.builtin.set_fact:
    wg_pubkey: "{{ 'COELt/q+/Fani0xOkmlxTTSBzm2pZJQDAtluqPP8e08=' | clevercloud.internal.wgpriv2pub }}"
    # Result is 'VIa3BtEFBVyDxWupO4I+T1bL0m0OoFsK/Fih9iogAgU='
'''

RETURN = '''
  _value:
    description: The wireguard public key
    type: str
'''

import subprocess
import shutil
from ansible.module_utils.common.text.converters import to_native
from ansible.errors import AnsibleError

def wgpriv2pub(wg_privkey):

    if not shutil.which("wg"):
        raise AnsibleError(
            f"Please install wireguard tools on node controller prior to use this filter")

    try:
        wg_pubkey = subprocess.check_output(
            f"echo '{wg_privkey}' | wg pubkey", shell=True, stderr=subprocess.PIPE).decode("utf-8").strip()
    except subprocess.CalledProcessError as e:
        raise AnsibleError(to_native(e.stderr))
    except Exception as e:
        raise AnsibleError(
            f"An unexpected error occured with wg cli: {to_native(e)}")

    return wg_pubkey


class FilterModule(object):
    '''Get wireguard public key from a private one'''

    def filters(self):
        return {
            'wgpriv2pub': wgpriv2pub
        }
